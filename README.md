container CI pipeline
===

Repository for common Gitlab CI rules to build container images.

Features:

* defines *test* / *build* / *container-test* / *release* stages, to
  make it possible to run tests before and after (integration tests)
  the image has been built
* transparently supports the [Gitlab dependency
  proxy](https://docs.gitlab.com/ee/user/packages/dependency_proxy)
  for container images hosted on Docker Hub, to avoid hitting rate
  limits

# Usage

Set up a project's *.gitlab-ci.yml* file with this include rule:

```yaml
include: "https://git.autistici.org/pipelines/containers/raw/master/common.yml"
```

